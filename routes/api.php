<?php

use App\Http\Controllers\ItemController;
use App\Http\Controllers\OfferController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'items', 'as' => 'items'], function () {
    Route::get('/', ItemController::class . '@index');
    Route::post('/', ItemController::class . '@store');
});

Route::group(['prefix' => 'offer', 'as' => 'offer'], function () {
    Route::get('/', OfferController::class . '@viewOffers');
    Route::post('/', OfferController::class . '@createOffer');
    Route::post('/accept', OfferController::class . '@acceptOffer');
});
