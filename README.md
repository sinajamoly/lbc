<p align="center" ><img decoding="async" width='300px' src="https://mliaavrbmlgu.i.optimole.com/bS1pDGE-aXE8bACZ/w:auto/h:auto/q:90/https://lbcstudios.ca/wp-content/uploads/2021/01/LBC-Logo-Colored.png" alt="LBC Studios"></p>

# About this

please contact me if you ran into any issue setting this up, this documentation should help setting up the app on your local env

## Setup

- Clone this into your local ENV
- Run `composer install`
- Run `composer dumpautoload`
- Go to database directory, remove database.sqlite and create it again to have a fresh database
- Run `php artisan key:generate`
- In the `.env` file set `DB_DATABASE` to absolute path of your local for the sqlite database 
- Run `php artisan migrate --database=sqlite`
- Run `php artisan serve` to run the server
- Postman collection has been provided


## test setup

- Go to database directory, remove test_database.sqlite and create it again to have a fresh test database
- In the `.env` file set `TESTING_DB_DATABASE` to absolute path of your local for the sqlite database 
- Run `php artisan migrate --database=testing`
- Run `php artisan test` to run test
- To run specific test run `php artisan test --filter TEST_CLASS_NAME`

## Area to improve
- Using fractals for having uniform response structure
- Using ORMs features to fetch and persist data 
- database field like owner should change to owner_id to follow all best practices and conventions
