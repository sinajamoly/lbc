<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('owner');
            $table->unsignedBigInteger('sold_to')->nullable(true);
            $table->unsignedBigInteger('item_id');
//            if the offer can be deleted before get sold OR having soft delete
//            $table->boolean('active')->default(true);
            $table->float('price')->nullable(false)->default(0.00);

            $table->foreign('owner')->references('id')->on('players');
            $table->foreign('sold_to')->references('id')->on('players');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
