<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\Offer;
use App\Models\player;
use Illuminate\Database\Eloquent\Factories\Factory;

class OfferFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Offer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $owner = player::factory()->create();
        $item = Item::factory(['owner' => $owner->id])->create();

        return [
            'owner' => $owner->id,
            'item_id' => $item->id,
            'price' => $this->faker->randomFloat(2, 10, 20)
        ];
    }
}
