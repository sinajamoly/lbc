<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class ItemRepository
{
    public static function changeItemOwner(int $itemId, int $newOwner): bool
    {
        return DB::update(
            'UPDATE items SET owner = :newOwner WHERE id = :id',
            ['newOwner' => $newOwner, 'id' => $itemId]
        );
    }

    public static function getItem(int $itemId): ?object
    {
        return DB::selectOne(
            DB::raw('SELECT owner from items where id = :item_id'),
            ['item_id' => $itemId]
        );
    }
}
