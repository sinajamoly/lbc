<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class PlayerRepository
{
    public static function getNRandomPlayer(int $numberOfPlayers): array
    {
        return DB::select(DB::raw('SELECT * FROM players ORDER BY RANDOM() LIMIT :n'), ['n' => $numberOfPlayers]);
    }

    public static function getPlayer(int $playerId): object
    {
        return DB::selectOne('SELECT * FROM players WHERE id = :playerId', ['playerId' => $playerId]);
    }

    public static function updatePlayerBalance(int $playerId, float $newBalance): bool
    {
        return DB::update(
            'UPDATE players SET balance = :newBalance WHERE id = :id',
            ['newBalance' => $newBalance, 'id' => $playerId]
        );
    }
}
