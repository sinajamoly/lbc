<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class OfferRepository
{
    public static function getOffers(array $playerIds): array
    {
        $playerIds = implode(',', $playerIds);
        return DB::select(
            'SELECT * FROM offers WHERE owner IN (' . $playerIds . ') AND sold_to IS NULL'
        );
    }

    public static function getOffer(int $offerId): ?object
    {
        return DB::selectOne("SELECT * FROM offers WHERE id = :offerId", ['offerId' => $offerId]);
    }

    public static function sellOfferTo(int $offerId, int $playerId): bool
    {
        return DB::update(
            'UPDATE offers SET sold_to = :playerId WHERE id = :id',
            ['playerId' => $playerId, 'id' => $offerId]
        );
    }

    public static function createOffer(int $owner, int $itemId, float $price): bool
    {
        return DB::insert(
            DB::raw('INSERT INTO offers (owner, item_id, price) VALUES (:owner, :item_id, :price)'),
            [
                'owner' => $owner,
                'item_id' => $itemId,
                'price' => $price,
            ]
        );
    }

    // just for demonstration
    public static function getOffersWithOneQuery(int $numberOfPlayers)
    {
        return DB::select(
            'SELECT * FROM offers WHERE owner IN (SELECT id from players LIMIT :n) AND sold_to IS NULL',
            ['n' => $numberOfPlayers]
        );
    }

    public static function getActiveOfferOfItem(int $itemId): array
    {
        return DB::select(
            DB::raw('SELECT * from offers where item_id = :item_id AND sold_to IS NULL'),
            ['item_id' => $itemId]
        );
    }
}
