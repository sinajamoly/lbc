<?php

namespace App\Services\Offer;

use App\Exceptions\NotEnoughBalanceException;
use App\Repositories\ItemRepository;
use App\Repositories\OfferRepository;
use App\Repositories\PlayerRepository;

final class AcceptOfferService
{
    public static function handle(int $buyerId, int $offerId): bool
    {
        $buyerPlayer = PlayerRepository::getPlayer($buyerId);
        $offer = OfferRepository::getOffer($offerId);
        $sellerPlayer = PlayerRepository::getPlayer($offer->owner);

        if ($buyerPlayer->balance < $offer->price) {
            throw new NotEnoughBalanceException();
        }
        OfferRepository::sellOfferTo($offer->id, $buyerPlayer->id);

        // add to seller balance
        PlayerRepository::updatePlayerBalance($offer->owner, $sellerPlayer->balance + $offer->price);

        // deduct from buyer balance
        PlayerRepository::updatePlayerBalance($buyerPlayer->id, $buyerPlayer->balance - $offer->price);

        ItemRepository::changeItemOwner($offer->item_id, $buyerPlayer->id);

        return true;
    }
}
