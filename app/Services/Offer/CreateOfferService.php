<?php

namespace App\Services\Offer;

use App\Repositories\OfferRepository;

final class CreateOfferService
{
    public static function handle(int $owner, int $itemId, float $price): bool
    {
        return OfferRepository::createOffer(
            $owner,
            $itemId,
            $price
        );
    }
}
