<?php

namespace App\Services\Offer;

use App\Repositories\OfferRepository;
use App\Repositories\PlayerRepository;
use Illuminate\Support\Arr;

class ViewOfferService
{
    public static function handle(int $randomNPLayer): array
    {
        $players = PlayerRepository::getNRandomPlayer($randomNPLayer);

        $playerIds = Arr::pluck($players, 'id');

        $playerIds = array_map('intval', $playerIds);

        return OfferRepository::getOffers($playerIds);
    }

    public static function handleAlternative(int $randomNPLayer): array
    {
        return OfferRepository::getOffersWithOneQuery($randomNPLayer);
    }
}
