<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $casts = [
        'owner' => 'integer',
        'price' => 'float',
        'item_id' => 'integer'
    ];

    protected $guarded = [];

    public function owner()
    {
        return $this->belongsTo(player::class, 'owner', 'id');
    }

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function soldTo()
    {
        return $this->belongsTo(player::class, 'sold_to', 'id');
    }
}
