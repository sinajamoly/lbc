<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $casts = [
        'owner' => 'integer'
    ];

    protected $guarded = [];

    public function owner()
    {
        return $this->belongsTo(player::class, 'owner', 'id');
    }

    public function offer()
    {
        return $this
            ->hasMany(Offer::class, 'item_id', 'id')
            ->where('sold_to', '=', null);
    }
}
