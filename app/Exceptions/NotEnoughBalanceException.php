<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class NotEnoughBalanceException extends Exception
{
    public function render()
    {
        return response()->json(['error' => 'You dont have enough balance.'], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
