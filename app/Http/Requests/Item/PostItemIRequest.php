<?php

namespace App\Http\Requests\Item;

use App\Models\player;
use Illuminate\Foundation\Http\FormRequest;

class PostItemIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'owner' => 'required|exists:' . player::class . ',id',
            'name' => 'required|string|MAX:25',
        ];
    }
}
