<?php

namespace App\Http\Requests\Offer;

use App\Models\Offer;
use App\Repositories\OfferRepository;
use Illuminate\Foundation\Http\FormRequest;

class AcceptOfferIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->has('player_id');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer_id' => [
                'required',
                'exists:' . Offer::class . ',id',
                $this->validatePlayerDontBuySoldOROwnItem(),
            ]
        ];
    }

    private function validatePlayerDontBuySoldOROwnItem()
    {
        return function ($attribute, $value, $fail) {
            $offer = OfferRepository::getOffer($value);
            if ($offer->owner == $this->get('player_id')) {
                $fail('You can not buy your own item.');
            }

            if ($offer->sold_to !== null) {
                $fail('You cannot purchase sold item.');
            }
        };
    }
}
