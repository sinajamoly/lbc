<?php

namespace App\Http\Requests\Offer;

use App\Repositories\ItemRepository;
use App\Repositories\OfferRepository;
use Illuminate\Foundation\Http\FormRequest;

class CreateOfferIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->has('player_id');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id' =>
                [
                    'required',
                    $this->validateUserNotSellingSomeoneElseItem(),
                    $this->validateUserDoesNotHaveExistingOfferOnThisItem(),
                ],
            'price' => ['required', 'numeric']
        ];
    }

    private function validateUserDoesNotHaveExistingOfferOnThisItem (): callable
    {
        return function ($attribute, $value, $fail) {
            $items = OfferRepository::getActiveOfferOfItem($value);

            if (\count($items)) {
                $fail('There is an existing offer on this item, you cannot make another offer.');
            }
        };
    }

    private function validateUserNotSellingSomeoneElseItem(): callable
    {
        return function ($attribute, $value, $fail) {
            $itemId = (int) ItemRepository::getItem($value)->owner;

            if ($itemId !== (int) $this->get('player_id')) {
                $fail('This Item does not belong to you, you can sell it.');
            }
        };
    }
}
