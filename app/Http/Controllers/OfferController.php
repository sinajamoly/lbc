<?php

namespace App\Http\Controllers;

use App\Http\Requests\Offer\AcceptOfferIRequest;
use App\Http\Requests\Offer\CreateOfferIRequest;
use App\Http\Requests\Offer\ViewOffersIRequest;
use App\Services\Offer\AcceptOfferService;
use App\Services\Offer\CreateOfferService;
use App\Services\Offer\ViewOfferService;
use Illuminate\Http\Response;

class OfferController extends Controller
{
    public function createOffer(CreateOfferIRequest $request)
    {
        CreateOfferService::handle(
            $request->get('player_id'),
            $request->get('item_id'),
            $request->get('price')
        );

        return \response()->json([
            'message' => 'Offer has been create successfully.'
        ], Response::HTTP_CREATED);
    }

    public function viewOffers(ViewOffersIRequest $request)
    {
//        $offers = ViewOfferService::handle((int) $request->get('n'));
        $offers = ViewOfferService::handleAlternative((int) $request->get('n'));

        return \response($offers, Response::HTTP_OK);
    }

    public function acceptOffer(AcceptOfferIRequest $request)
    {
        AcceptOfferService::handle($request->get('player_id'), $request->get('offer_id'));

        return \response()->json(['item is yours now'], Response::HTTP_OK);
    }
}
