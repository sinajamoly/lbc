<?php

namespace App\Http\Controllers;

use App\Http\Requests\Item\IndexItemIRequest;
use App\Http\Requests\Item\PostItemIRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function index(IndexItemIRequest $request)
    {
        $items = DB::select(
            DB::raw('SELECT * FROM items where owner = :player_id'),
            ['player_id' => $request->get('player_id')]
        );

        return \response($items, Response::HTTP_OK);
    }

    public function store(PostItemIRequest $request)
    {
        DB::insert(
            DB::raw('INSERT INTO items (name, owner) VALUES (:name, :owner)'),
            ['name' => $request->get('name'), 'owner' => $request->get('owner')]
        );

        return \response(Response::HTTP_CREATED);
    }
}
