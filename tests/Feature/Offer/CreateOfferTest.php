<?php

namespace Tests\Feature\Offer;

use App\Models\Item;
use App\Models\Offer;
use App\Models\player;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateOfferTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateOfferSuccessfully()
    {
        $player = player::factory()->create();
        $item = Item::factory()->create(['owner' => $player->id]);

        $response = $this->postJson(
            '/api/offer/?player_id='. $player->id ,
            ['item_id' => $item->id, 'price' => 150]
        );

        $this->assertContains('Offer has been create successfully.', $response->json());

        $response->assertStatus(Response::HTTP_CREATED);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSellingSomeoneElseItem()
    {
        $player = Player::factory()->create();
        $item = Item::factory()->create(['owner' => $player->id]);

        $hacker = player::factory()->create();

        $response = $this->postJson(
            '/api/offer/?player_id='. $hacker->id ,
            ['item_id' => $item->id, 'price' => 150]
        );

        $response->assertSee('This Item does not belong to you, you can sell it.');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMakingOfferOnItemWhichAlreadyHasOffer()
    {
        $player = Player::factory()->create();
        $item = Item::factory()->create(['owner' => $player->id]);
        Offer::factory()->create(['owner' => $player->id, 'item_id' => $item->id, 'price' => 200.00]);

        $response = $this->postJson(
            '/api/offer/?player_id='. $player->id ,
            ['item_id' => $item->id, 'price' => 150]
        );

        $response->assertSee('There is an existing offer on this item, you cannot make another offer.');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
