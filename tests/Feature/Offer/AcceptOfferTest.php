<?php

namespace Tests\Feature;

use App\Models\Item;
use App\Models\Offer;
use App\Models\player;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class AcceptOfferTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAcceptingAnSuccessfully()
    {
        $sellerInitialBalance = 1000.00;
        $seller = Player::factory()->create(['balance' => $sellerInitialBalance]);

        $item = Item::factory()->create(['owner' => $seller->id]);

        $offerPrice = 200.00;
        $offer = Offer::factory()->create(['owner' => $seller->id, 'item_id' => $item->id, 'price' => $offerPrice]);

        $buyerInitialBalance = 500.00;
        $buyer = PLayer::factory()->create(['balance' => $buyerInitialBalance]);

        $response = $this->postJson('/api/offer/accept?player_id=' . $buyer->id, ['offer_id' => $offer->id]);

        $this->assertEquals($offer->refresh()->sold_to, $buyer->id);
        $this->assertEquals($seller->refresh()->balance, $sellerInitialBalance + $offerPrice);
        $this->assertEquals($buyer->refresh()->balance, $buyerInitialBalance - $offer->price);
        $this->assertEquals($item->refresh()->owner, $item->owner);

        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAcceptingOwnOffer()
    {
        $sellerInitialBalance = 1000.00;
        $seller = Player::factory()->create(['balance' => $sellerInitialBalance]);

        $item = Item::factory()->create(['owner' => $seller->id]);

        $offerPrice = 200.00;
        $offer = Offer::factory()->create(['owner' => $seller->id, 'item_id' => $item->id, 'price' => $offerPrice]);

        $response = $this->postJson('/api/offer/accept?player_id=' . $seller->id, ['offer_id' => $offer->id]);

        $response->assertSee('You can not buy your own item.');
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAcceptingAcceptedOffer()
    {
        $seller = Player::factory()->create();
        $buyer = PLayer::factory()->create();
        $item = Item::factory()->create(['owner' => $buyer->id]);

        $offer = Offer::factory()->create([
            'owner' => $seller->id,
            'item_id' => $item->id,
            'price' => 200.00,
            'sold_to' => $buyer->id
        ]);

        $anotherPlayer = Player::factory()->create();

        $response = $this->postJson('/api/offer/accept?player_id=' . $anotherPlayer->id, ['offer_id' => $offer->id]);

        $response->assertSee('You cannot purchase sold item.');
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testNotHavingEnoughBalance()
    {
        $sellerInitialBalance = 1000.00;
        $seller = Player::factory()->create(['balance' => $sellerInitialBalance]);

        $item = Item::factory()->create(['owner' => $seller->id]);

        $offerPrice = 700.00;
        $offer = Offer::factory()->create(['owner' => $seller->id, 'item_id' => $item->id, 'price' => $offerPrice]);

        $buyerInitialBalance = 500.00;
        $buyer = PLayer::factory()->create(['balance' => $buyerInitialBalance]);

        $response = $this->postJson('/api/offer/accept?player_id=' . $buyer->id, ['offer_id' => $offer->id]);

        $response->assertSee('You dont have enough balance.');
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
