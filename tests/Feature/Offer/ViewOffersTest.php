<?php

namespace Tests\Feature\Offer;

use App\Models\Offer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class ViewOffersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testReturnOffersFromNDifferentPlayer()
    {
        // number of offers from different players
        $offersNumber = 10;
        Offer::factory()->count($offersNumber)->create();

        // act
        $response = $this->get('/api/offer?player_id=1&n=' . $offersNumber);

        // assert
        $this->equalTo(count($response->json()), $offersNumber);
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testReturnSuccessWhenThereIsNoOfferExist()
    {
        // number of offers from different players
        $offersNumber = 10;

        // act
        $response = $this->get('/api/offer?player_id=1&n=' . $offersNumber);

        // assert
        $this->equalTo(count($response->json()), 0);
        $response->assertStatus(Response::HTTP_OK);
    }
}
